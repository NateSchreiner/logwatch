package main

import (
	"fmt"

	"gitlab.com/NateSchreiner/logwatch/config"
)

func main() {
	config := config.LoadFromFile("/Users/nateschreiner/fun/logwatch/config.yaml")
	fmt.Println(config)
}
