module gitlab.com/NateSchreiner/logwatch

go 1.21.0

require (
	golang.org/x/exp v0.0.0-20230817173708-d852ddb80c63 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
