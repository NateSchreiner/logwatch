package main

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/NateSchreiner/logwatch/config"
	"gitlab.com/NateSchreiner/logwatch/logwatch"
	"golang.org/x/exp/slog"
)

func main() {
	if len(os.Args) < 2 {
		slog.Error("Command", "argument-error", "cli command requires config path")
		return
	}

	path := os.Args[1]
	slog.Info("init", "config-path", path)
	config := config.LoadFromFile(path)

	lw := logwatch.NewWatcher(config)
	slog.Info("Main", "After logwatch init")

	lw.Monitor()
}

func testBigFileRead() {
	pos := int64(20)
	f, err := os.Open("/Users/nateschreiner/projects/logwatch/testfile.txt")
	if err != nil {
		panic(err)
	}

	f.Seek(pos, 0)

	buf := make([]byte, 16)

	n, err := f.Read(buf[:cap(buf)])
	if err != nil {
		if err != io.EOF {
			panic(err)
		}
	}

	fmt.Printf("Read %d bytes\n", n)
	fmt.Println(string(buf))
}
