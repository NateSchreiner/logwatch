package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Remote    map[string]string
	WatchDirs []string `yaml:"watch"`
	Database  map[string]string
}

func LoadFromFile(path string) *Config {
	config := new(Config)

	bytes, err := os.ReadFile(path)
	if err != nil {
		panic(err)
		// log.Fatalf("LoadConfig", "read-error", err.Error())
	}

	err = yaml.Unmarshal(bytes, &config)
	if err != nil {
		panic(err)
		// log.Fatalf("LoadConfig", "unmarshal-error", err.Error())
	}

	fmt.Println(config.WatchDirs)
	return config
}
