package logwatch

import (
	"os"
	"strings"
	"testing"

	"gitlab.com/NateSchreiner/logwatch/config"
)

// watch file, write to it, watcher picks it up (Check what BYTES get sent)
// watch file, delete line, what does watcher do? does it send a delete?

// send Sender a list of trackers,

func testLogwatchWatcher(t *testing.T) {
	var tests = []struct {
		file         string
		originalData string
		finalData    string
	}{
		{"testOne.txt",
			"This is the original file.  It's got some data in it, and will obviously have more data in it after the test",
			"This is the original file.  It's got some data in it, and will obviously have more data in it after the test. This is the last line in the file.",
		},
		// Test case to see about removal of lines?
		{
			"testOne.txt",
			"This is the original file.  It's got some data in it, and will obviously have more data in it after the test",
			"This is the original file.  It's got some data in it",
		},
		// Test case for a deleted file?
		{
			"testOne.txt",
			"This is the original file.  It's got some data in it, and will obviously have more data in it after the test",
			"",
		},
	}

	for _, tt := range tests {
		// make file with fn
		fn := tt.file
		file, err := os.Create(fn)
		if err != nil {
			t.Fatalf("could not create test file. ", err.Error())
		}
		// write to file with text
		text := tt.originalData
		n, err := file.Write([]byte(text))
		if err != nil {
			t.Fatalf("could not write to test file. ", err.Error())
		}

		if n != len(text) {
			t.Fatalf("did not write 100% of the bytes to test file. Wrote=%d, expeted=%d", n, len(text))
		}
		// Start logwatch
		config := &config.Config{
			Remote:    nil,
			WatchDirs: []string{fn},
			Database:  nil,
		}

		l := NewWatcher(config)
		l.Monitor()
		// Write new data to text file
		file.Write()

		// Check that logwatch has this file in it's queue with proper differences
		// (Only the difference between tt.originalData and tt.finalData)

		// Delete file
	}
}

func diff(a, b string) string {
	if strings.Contains(a, b) {
		if strings.Contains(b, a) {
			// Both strings are equal?
		} else {
			// (a) has more, (b) is a substring of (a)
		}

	}
}
