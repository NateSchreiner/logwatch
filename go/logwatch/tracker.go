package logwatch

import "time"

type Tracker struct {
	Name     string
	ModTime  time.Time
	LastRead int64
	Hash     []byte
}
