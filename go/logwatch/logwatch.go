package logwatch

import (
	"bytes"
	"crypto/sha1"
	"encoding/json"
	"io"
	"math"
	"net/http"
	"os"
	"sync"

	"gitlab.com/NateSchreiner/logwatch/config"
	"golang.org/x/exp/slog"
)

const filechunk = 8192

type Logwatch struct {
	// Logwatch will run two methods concurrently
	config *config.Config
	// Buffer for files that need sending
	buffer map[string]struct{}
	// Need DB for in-memory hashes
	Database map[string]string
	bufMutex sync.Mutex
	Trackers []*Tracker
}

func NewWatcher(config *config.Config) *Logwatch {
	lw := &Logwatch{
		config: config,
		buffer: make(map[string]struct{}, 0),
	}

	lw.init()

	return lw
}

// TODO :: What is LastReadByte? Do we query server
func (lw *Logwatch) init() {

	r := make([]*Tracker, 0)

	for _, path := range lw.config.WatchDirs {
		fi, err := os.Stat(path)
		if err != nil {
			panic(err)
		}

		hash := computeHash(path)

		t := &Tracker{
			Name:     path,
			ModTime:  fi.ModTime(),
			LastRead: int64(0),
			Hash:     hash,
		}

		r = append(r, t)
	}

	lw.Trackers = r
}

// Incrementally computes a hash.  Can be used for large files
// since the hash is incrementally computed instead of reading entire
// file into memory
func computeHash(path string) []byte {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}

	info, err := f.Stat()
	if err != nil {
		panic(err)
	}

	filesize := info.Size()
	blocks := uint64(math.Ceil(float64(filesize) / float64(filechunk)))

	hash := sha1.New()

	for i := uint64(0); i < blocks; i++ {
		// Calculate block size
		blocksize := int(math.Min(filechunk, float64(filesize-int64(i*filechunk))))

		buf := make([]byte, blocksize)

		f.Read(buf)

		io.WriteString(hash, string(buf))
	}

	return hash.Sum(nil)

}

// Basically need to re-implement `tail -f` except instead of
// outputting to stdout we send over the wire to the configured
// server (or other configurable source?)
//
// TODO ::
// 1. Init - Iterates over watched files
// 2. Stat each file and save in memory (Mod time, size, lastRead byte)
// 3. Start a loop in background that monitors files for changes
// 4. When it finds a modified file (Size, mod time?) update in mem object
// 5. Add to a send buffer to send out all bytes after LastReadByte
func (l *Logwatch) Monitor() {
	for {
		toSend := l.checkTrackers()
		// slog.Info("Monitor", "trackers-to-send", len(toSend))
		l.send(toSend)
	}
}

func (l *Logwatch) checkTrackers() []*Tracker {

	r := make([]*Tracker, 0)

	for _, t := range l.Trackers {
		fi, err := os.Stat(t.Name)
		if err != nil {
			panic(err)
		}

		// TODO :: This will always be true on start
		if fi.ModTime().Equal(t.ModTime) {
			continue
		}

		newhash := computeHash(t.Name)

		if !bytes.Equal(newhash, t.Hash) {
			r = append(r, t)
			t.Hash = newhash
			t.ModTime = fi.ModTime()
			t.LastRead = fi.Size()
		}
	}

	return r
}

func (l *Logwatch) tailF() {
	// We need some sort of object for memory.  Remember the last position of
	// the file we read.  Last Modifcation time. Size of file.  Hash of file.
	for {
		for _, file := range l.config.WatchDirs {
			hash := computeHash(file)
			if l.Database[file] != string(hash) {
				_, ok := l.Database[file]
				if !ok {
					l.Database[file] = string(hash)
					l.append(file)
				}
			}
		}
	}
}

func (l *Logwatch) watch() {
	slog.Info("Watch", "watch() called...")
	files := l.config.WatchDirs
	for {
		for _, file := range files {
			hash := computeHash(file)
			if l.Database[file] != string(hash) {
				_, ok := l.Database[file]
				if !ok {
					l.Database[file] = string(hash)
					l.append(file)
				}
			}
		}
	}
}

func (l *Logwatch) append(fp string) {
	l.bufMutex.Lock()

	_, ok := l.buffer[fp]
	if !ok {
		l.buffer[fp] = struct{}{}
	}

	l.bufMutex.Unlock()
}

func (l *Logwatch) send(items []*Tracker) {
	// establih connection
	url := l.config.Remote["url"]
	body := make([]byte, 0)

	for _, i := range items {
		f, err := os.Open(i.Name)
		if err != nil {
			panic(err)
		}
		f.Seek(i.LastRead, 0)
		buf := make([]byte, 1024)
		for {
			_, err := f.Read(buf)
			if err != nil {
				if err == io.EOF {
					body = append(body, buf...)
					break
				}
			}

			body = append(body, buf...)
		}

		// TODO:: We want the remote server to have a meaningful directory.
		req, err := http.NewRequest("POST", url, bytes.NewBuffer(body))
		if err != nil {
			panic(err)
		}

		query := req.URL.Query()
		query.Add("action", "append")
		// TODO :: Need path
		query.Add("path", "")
		req.URL.RawQuery = query.Encode()

		client := &http.Client{}
		res, err := client.Do(req)
		if err != nil {
			panic(err)
		}

		defer res.Body.Close()
		if res.StatusCode != 200 {
			body := readBody(res.Body)
			slog.Error("send-file-error", i.Name, body)
		}
	}

}

func readBody(body io.ReadCloser) string {
	buf := make([]byte, 0)
	for {
		b := make([]byte, 1)
		_, err := body.Read(b)
		if err != nil {
			if err == io.EOF {
				break
			}
		}

		buf = append(buf, b...)
	}

	return string(buf)
}

func (l *Logwatch) sendTwo() {
	slog.Info("send", "send() called...")
	count := 0
	for {
		// fmt.Println("Send")
		if count%10 == 0 {
			slog.Info("Send", "buf-count", len(l.buffer))
		}
		if len(l.buffer) > 0 {
			f, err := os.OpenFile("/Users/nateschreiner/fun/logwatch/buffer_file.log", os.O_WRONLY, 0)
			for key, _ := range l.buffer {
				if err != nil {
					panic(err)
				}
				b := []byte(key)
				b = append(b, '\n')
				f.Write(b)
			}
			f.Close()
		}
		count++
	}
}

// Read database file from specified path in `config` into Logwatch
// Memory.
func (l *Logwatch) initDb() {
	path := l.config.Database["path"]
	watchFiles := l.config.WatchDirs

	db, err := os.OpenFile(path, os.O_WRONLY, 0)
	if err != nil {
		panic(err)
	}

	// TODO :: Check that there are no entries in DB file arleady
	// if there are, parse the file, load into memory
	for _, path := range watchFiles {
		hash := computeHash(path)
		l.Database = make(map[string]string, 0)
		l.Database[path] = string(hash)
	}

	j, e := json.Marshal(l.Database)
	if e != nil {
		panic(e)
	}

	n, e := db.Write(j)
	if e != nil {
		panic(e)
	}

	slog.Info("InitDB", "Wrote bytes", n)
}
