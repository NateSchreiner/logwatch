from httpfs_client import Client

def setup():
    url = 'http://localhost:8080/'
    return Client(url)

def testGetFile():
    client = setup()
    file = client.getFile('work-dir/append/test.txt')
    print(file)

def testCreateDir():
    client = setup()
    response = client.createDir('python/test')
    print(f"Found status code: {response.status_code}")

def testAppendFile():
    client = setup()
    response = client.appendFile('Test string that I want appended', 'python/test/test.txt')
    print(f"Status Code: {response.status_code}")

testAppendFile()