import requests

class Client(object):
    def __init__(self, url, config=None):
        self._url = url
        self._config = config

    def getFile(self, path):
        params = {
            'action': 'read',
            'path': path
        }
        r = requests.get(url = self._url, params = params)
        return r.json()

    def appendFile(self, data, path):
        params = {
            'action': 'append',
            'path': path
        }

        r = requests.post(url = self._url, params=params, data=data)
        return r

    def createDir(self, path):
        params = {
            'action': 'create',
            'path': path 
        }

        r = requests.put(url = self._url, params = params)
        return r
    
    def createFile(self, filename, path, file):
        params = {
            'action': 'upload',
            'path': path,
            'filename': filename,
            'file': file
        }

        r = requests.put(url = self._url, params = params)
        return r

