import yaml
import psutil
from multiprocessing import Process
import asyncio
import os
import hashlib
import json
import sys

class LogWatch(object):
    def __init__(self, config, db_path, client):
        with open(config, 'r') as f:
            try:
                tmp = yaml.safe_load(f)
            except yaml.YAMLError as e:
                sys.exit(e)

        self._config = tmp
        self._db_path = db_path
        self._client = client
        self.buffer = {}

    async def job(self, loop):
        watch = loop.create_task(self.watch())
        send = loop.create_task(self.send())
        await asyncio.wait([watch, send])

    def watch(self):
        print("Watching...")
        with open(self._db_path, 'r') as db:
            # Pull DB into memory
            # Every so often write the content of memory 
            # to file incase of crashes?

            # TODO :: What if DB is empty?
            try:
                self._db = json.load(db)
            except:
                self.initHashes()
                self._db = json.load(db)
            # 1 - Iterate over watched files
            for path, hash in self._db.items():
                hash = self.computeHash(path)
                if self._db[path] != hash:
                    print(f"Found mismatch hash for: {os.path.basename(path)}")
                    # New lines update in memory hash
                    self._db[path] = hash
                    # Send new line off to log server
                    # should probably buffer this
                    if path not in self.buffer:
                        self.buffer[path] = path
        print(f"Watch Buf Size: {len(self.buffer)}")

    def send(self):
        print("Running send...")
        cur_send_pid = None
        while True:
            print(f"Buf size: {len(self.buffer)}")
            if len(self.buffer) > 0:
                for filename in self.buffer.keys():
                    print(f"Found {filename} in buffer...")
                    file = open(path, 'rb')
                    path = self.buffer.pop(filename)
                    response = self._client.createFile(filename, path, file)
                    print(response)
            else:
                if cur_send_pid is None or not psutil.pid_exists(cur_send_pid):
                    p = Process(target=self.watch)
                    p.daemon = True
                    p.start()
                    cur_send_pid = p.pid

    def initHashes(self):
        hashes = {}
        for path in self._config['watch']:
            # print(f"Need to compute {path}")
            hash = self.computeHash(path)
            print(f"Got hash: {hash}")
            # fn = os.path.basename(path)
            hashes[path] = hash
        self.persistHashes(hashes)

    def persistHashes(self, hashes):
        h = dict(map(lambda kv: (kv[0], str(kv[1])), hashes.items()))
        print(f"Opening DB: {self._config['database']['path']}")
        with open(self._config['database']['path'], 'w') as f:
            n = f.write(json.dumps(h))
                    
    def computeHash(self, path):
        # print(f"Computing hash of: {path}")
        hash = hashlib.sha1()
        with open(path, 'rb') as f:
            while True:
                data = f.readline()
                if not data:
                    break
                hash.update(data)
        return hash.digest()


# Wednesday 23rd
