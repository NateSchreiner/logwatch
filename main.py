import hashlib
import asyncio
import json
from threading import Thread
from logwatch import *
from httpfs import httpfs_client

sha1 = hashlib.sha1()
filenames = {'access':'access.log', 'test':'test.log'}
url = 'http://localhost:8080/'

class Backend(object):
    def __init__(self):
        self._storage = "file"
        self._path = "/Users/nateschreiner/fun/logwatch/db.json"
        self.json = None

    def writeHashes(self, hashes):
        h = dict(map(lambda kv: (kv[0], str(kv[1])), hashes.items()))
        self.json = json.dumps(h)

    def persist(self, hashes):
        if self._storage == "file":
            with open(self._config['database']['path'], 'w') as f:
                n = f.write(self.json)
            

    def computeHash(self, path):
        hash = hashlib.sha1()
        with open(path, 'rb') as f:
            while True:
                data = f.readline()
                if not data:
                    break
                hash.update(data)
        return hash


def computeAllHashes():
    hashes = {}
    for key, val in filenames.items():
        with open(val, 'rb') as f:
            while True:
                data = f.readline()
                if not data:
                    break
                sha1.update(data)
            hashes[val] = sha1.digest()
    return hashes

def testBackend():
    backend = Backend()
    hashes = computeAllHashes()
    backend.writeHashes(hashes)
    backend.persist()

if __name__ == '__main__':

    client = httpfs_client.Client(url)
    config = "/Users/nateschreiner/fun/logwatch/config.yaml"
    db = "/Users/nateschreiner/fun/logwatch/db.json"
    watcher = LogWatch(config, db, client)
    watcher.send()

    # one = Thread(target=watcher.watch()).start()
    # two = Thread(target=watcher.send()).start()
    # loop = asyncio.new_event_loop()
    # loop.run_until_complete(watcher.job(loop))
    # loop = asyncio.set_event_loop(loop)
    # try:
    #     # loop.run(watcher.job(loop))
    #     # asyncio.run(loop)
    #     # loop.run_until_complete(watcher.job(loop))
    #     # loop.close()
    # except KeyboardInterrupt:
    #     pass
